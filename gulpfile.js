var gulp = require('gulp'),
    nightwatch = require('gulp-nightwatch');

var shell = require('gulp-shell')

// gulp.task('default', function() {
//    gulp.src()
//      .pipe(nightwatch({
//        // configFile: "nightwatch.json"
//      }
//    ));
// });

gulp.task('default', function () {
  return gulp.src('*.js', {read: false})
    .pipe(shell(['sh start_selenium.sh']))
    .pipe(shell(['node_modules/nightwatch/bin/nightwatch -t test-nightwatch.js']))
})

