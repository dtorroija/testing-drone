#E2E-tests for widget portal!


Purpose:
=======
this is an example of how to implement automation tests, is a first approach and only test the flashcard widget creation and access for now, but if this receive the proper space in metrodigi it will be for testing all the widgets!

know issues:
==========

chrome has a bug in the drag and drop with selenium, so for now, the tests are running in Firefox for now, but in the future the idea is to run in both browsers.

Start the tests
=======================================

in the command console run:

`npm test`



if you have any questions please email me at:
dtorroija@metrodigi.com